import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// Material UI
import { Button, CircularProgress, Grid, TextField } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

// Actions
import { calculate } from "../actions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    margin: {
      margin: theme.spacing(1),
    },
    paper: {
      color: theme.palette.text.secondary,
      padding: theme.spacing(2),
      textAlign: "center",
    },
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: 300,
      },
      "flexGrow": 1,
    },
  }),
);

export default function Form() {
  const classes = useStyles();

  const [ studiesPerday, setNumberOfStudiesPerday ] = useState("");
  const [ studiesPerdayError, setNumberOfStudiesPerdayError ] = useState(false);

  const [ studyGrowth, setNumberOfStudyGrowth ] = useState("");
  const [ studyGrowthError, setNumberOfStudyGrowthError ] = useState(false);

  const [ monthsToForecast, setNumberOfMonthsToForecast ] = useState("");
  const [ monthsToForecastError, setNumberOfMonthsToForecastError ] = useState(false);

  const dispatch = useDispatch();

  const status = useSelector( (state: any) => {
    return (state.status) ? state.status : null;
  });

  const validate = (): boolean => {

    let isValid: boolean = true;

    if ( !Number(studiesPerday) ) {
      setNumberOfStudiesPerdayError(true);
      isValid = false;
    } else {
      setNumberOfStudiesPerdayError(false);
    }
    if ( !Number(studyGrowth) ) {
      if (studyGrowth === "0") {
        setNumberOfStudyGrowthError(false);
      } else {
        setNumberOfStudyGrowthError(true);
        isValid = false;
      }
    } else {
      setNumberOfStudyGrowthError(false);
    }
    if ( !Number(monthsToForecast) ) {
      setNumberOfMonthsToForecastError(true);
      isValid = false;
    } else {
      setNumberOfMonthsToForecastError(false);
    }

    return isValid;
  }

  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (validate()) {
      dispatch(calculate(studiesPerday, studyGrowth, monthsToForecast));
    }
  };

  return (
    <div className={ classes.root }>
        <form onSubmit={handleSubmit} noValidate>
        <Grid container spacing={ 3 }>
          <Grid item xs={ 12 } sm={ 6 }>
              <div>
                <TextField
                  label="Number of study per day"
                  required
                  id="number-of-schools-per-day"
                  value={ studiesPerday }
                  onChange={ (event) => setNumberOfStudiesPerday(event.target.value) }
                  variant="outlined"
                  error={ studiesPerdayError }
                />
              </div>
          </Grid>
          <Grid item xs={ 12 } sm={ 6 }>
            <div>
                <TextField
                  label="Number of of study growth"
                  required
                  id="number-of-growth"
                  value={ studyGrowth }
                  onChange={ (event) => setNumberOfStudyGrowth(event.target.value)}
                  variant="outlined"
                  error={ studyGrowthError }
                />
              </div>
          </Grid>
          <Grid item xs={ 12 } sm={ 6 }>
            <div>
                <TextField
                  label="Number of months to forecast"
                  required
                  id="number-of-months"
                  value={ monthsToForecast }
                  onChange={ (event) => setNumberOfMonthsToForecast(event.target.value)}
                  variant="outlined"
                  error={ monthsToForecastError }
                />
              </div>
          </Grid>
          <Grid item xs={ 12 } sm={ 6 }>
            { (status !== "busy") ? (
              <Button
                type="button"
                variant="contained"
                onClick={(event) => { handleSubmit(event); }}>Calculate
              </Button>
            )  :  (
              <CircularProgress />
            )}
          </Grid>
        </Grid>
        </form>
    </div>
  );
}
