import axios from "axios";
import { API_URL } from "../common/";

export const CustomerUsage = {
  /**
   * Makes an API call to calculate forecast
   */
  calculateForecast: (monthsToForecast: string, studiesPerDay: string, studyGrowth: string) => {

    return axios.get(`${API_URL}/forecast?month_to_forecast=${monthsToForecast}&studies_per_day=${studiesPerDay}&study_growth=${studyGrowth}`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        console.log("THere was an error making the API call: ", error);
        return null;
      });
  },
};
