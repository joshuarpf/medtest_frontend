import { DISPLAY_CALCULATIONS, STATUS } from "../common/actionTypes";

const initialState = { status: "" };

const reducer = ( state = initialState, action: any ): any => {
  switch(action.type) {
    case DISPLAY_CALCULATIONS:
      return { ...state, data: action.paylaod };
    case STATUS:
      return { ...state, status: action.payload };
    default:
      return state;
  }
};

export default reducer;
