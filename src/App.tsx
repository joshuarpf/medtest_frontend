import React from "react";

// Material UI
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

// Components
import FormComponent from "./components/FormComponent";
import TableComponent from "./components/TableComponent";

const useStyles = makeStyles((theme) => ({
  App: {
    padding: "50",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  paper: {
    color: theme.palette.text.secondary,
    padding: theme.spacing(2),
    textAlign: "center",
  },
  root: {
    flexGrow: 1,
  },
}));

function App() {

  const classes = useStyles();

  return (
    <div className={ classes.App }>
      <Container maxWidth="md" className={ classes.root }>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper className={ classes.paper }>
              <AppBar position="static">
              <Toolbar>
                <IconButton edge="start" className={ classes.menuButton } color="inherit" aria-label="menu">
                </IconButton>
                <Typography variant="h6">
                  Lifetrack Medical System
                </Typography>
              </Toolbar>
              </AppBar>
              <FormComponent />
            </Paper>
          </Grid>
          <Grid xs={ 12 }>
            <Paper className={ classes.paper }>
              <TableComponent />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default App;
