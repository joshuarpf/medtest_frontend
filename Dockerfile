FROM mhart/alpine-node:12.6.0
WORKDIR /app
COPY package.json /app/
RUN yarn
COPY . /app/
EXPOSE 80
CMD ["yarn", "start"]