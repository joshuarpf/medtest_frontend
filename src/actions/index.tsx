import { CALL_CALCULATE, DISPLAY_CALCULATIONS, STATUS } from "../common/actionTypes";

export const busy = (): any => (
  {
    payload: "busy",
    type: STATUS,
  }
);

export const displayResults = (data: [any]): any => (
  {
    paylaod: { forecast: data },
    type: DISPLAY_CALCULATIONS,
  }
);

export const calculate = (studiesPerDay: string, studyGrowth: string, monthsToForecast: string): any => (
  {
    payload: { monthsToForecast, studiesPerDay, studyGrowth },
    type: CALL_CALCULATE,
  }
);

export const notbusy = (): any => (
  {
    payload: "not busy",
    type: STATUS,
  }
);
