import React from 'react';
import { useSelector } from "react-redux";

// Material UI
import Paper from "@material-ui/core/Paper";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function CustomizedTables() {
  const classes = useStyles();

  const forecast = useSelector( (state: any) => {
    if (state.data && state.data.forecast) {
      return state.data.forecast;
    }

    return null;
  });

  return (
    <div>
      { (forecast) ? (
      <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Month of Year</StyledTableCell>
            <StyledTableCell>Number of Studies</StyledTableCell>
            <StyledTableCell>Cost Forecasted</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {forecast.map((row: any) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <StyledTableCell>{parseFloat(row.number_of_studies.toFixed(4))}</StyledTableCell>
              <StyledTableCell>{parseFloat(row.total_cost.toFixed(4))}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
      </TableContainer>
      ) : (
        <h2>Please fill in the form to generate</h2>
      )}
    </div>
  );
}
