import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import reducer from "./reducers";
import root from "./sagas";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const initializeSagaMiddleware = createSagaMiddleware();
const storeEnhancers = ( window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const Store = createStore(
  reducer,
  storeEnhancers(
    applyMiddleware(initializeSagaMiddleware),
  ),
);

initializeSagaMiddleware.run(root);

export default Store;
