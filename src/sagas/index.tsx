import { all, put, takeEvery } from "redux-saga/effects";
import { busy, displayResults, notbusy } from "../actions";
import { CALL_CALCULATE } from "../common/actionTypes";
import { CustomerUsage } from "../modules/CustomerUsage";

function* makeAPICall(action: any) {
  yield put(busy());

  try {

    const { monthsToForecast, studiesPerDay, studyGrowth } = action.payload;
    const forecast = yield CustomerUsage.calculateForecast(monthsToForecast, studiesPerDay, studyGrowth)
      .then( (result) => result).catch((error) => {
        console.error("There was a problem when calling API: ", error);
      });

    yield put(displayResults(forecast.data));
    yield put(notbusy());

  } catch (error) {
    yield put(notbusy());
  }
}

/**
 * Main action listener
 */
function* actionWatcher() {
  yield takeEvery(CALL_CALCULATE, makeAPICall);
}

/**
 * Root saga
 */
export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}
